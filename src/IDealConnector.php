<?php

namespace Drupal\commerce_ideal;

use Bs\IDeal\Exception\ResponseException as IDealResponseException;
use Bs\IDeal\IDeal;
use Drupal\commerce_payment\Exception\InvalidRequestException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\State\StateInterface;

/**
 * Class IDealConnector.
 *
 * @package Drupal\commerce_ideal
 */
class IDealConnector implements IDealConnectorInterface {

  use LoggerChannelTrait;
  use MessengerTrait;

  const GENERAL_ERROR = 'Unfortunately, it is not possible to pay using iDEAL at this time. Please try again later or use an alternative method of payment';

  /**
   * The configuration data from the plugin.
   *
   * @var array
   */
  protected $pluginConfig;

  /**
   * The iDEAL connector.
   *
   * @var \Bs\IDeal\IDeal
   */
  protected $connector;

  /**
   * The state storage.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The logger chanel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * PaymentRequestService constructor.
   *
   * @param array $config
   *   Configuration data from the plugin.
   * @param \Drupal\Core\State\StateInterface $state
   *   Drupal state storage.
   */
  public function __construct(array $config, StateInterface $state) {
    $this->pluginConfig = $config;
    $this->state = $state;
    $this->logger = $this->getLogger('commerce_ideal');

    $this->connector = new IDeal($config['acquire_url']);
    $this->connector->setAcquirerCertificate($config['path_to_acquirer_certificate'], TRUE);
    $this->connector->setMerchant($config['merchant_id'], $config['sub_id']);
    $this->connector->setMerchantCertificate($config['path_to_merchant_certificate'], TRUE);
    $this->connector->setMerchantPrivateKey($config['path_to_merchant_private_key'], $config['merchant_private_key_password'], TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function getConnector() {
    return $this->connector;
  }

  /**
   * {@inheritdoc}
   */
  public function getIssuers() {
    $issuers = $this->state->get('commerce_ideal.issuers_list', []);
    $last_update = $this->state->get('commerce_ideal.issuers_last_update');
    $period = (int) $this->pluginConfig['get_issuers_period'] ?: 1;
    $period *= 86400;

    if (empty($issuers) || (($last_update + $period) <= time())) {
      try {
        $request = $this->connector->createDirectoryRequest();
        $response = $request->send();
        $issuers = $response->getAllIssuers();
        $this->state->set('commerce_ideal.issuers_list', $issuers);
        $this->state->set('commerce_ideal.issuers_last_update', time());
      }
      catch (IDealResponseException $e) {
        $this->logger->error("Ideal directory request failed due to error: @error. Details: @details", ['@error' => $e->getErrorMessage(), '@details' => $e->getErrorDetail()]);
        throw new InvalidRequestException($e->getConsumerMessage());
      }
      catch (\Exception $e) {
        $this->logger->error("Ideal directory request failed due to error @error", ['@error' => $e->getMessage()]);
        throw new PaymentGatewayException(self::GENERAL_ERROR);
      }

    }

    return $issuers;
  }

  /**
   * {@inheritdoc}
   */
  public function sendTransactionRequest($issuer, $return_url, $purchase_id, $amount, $description) {
    try {
      // Description field must not contain characters that can lead to problems
      // (for example those occurring in HTML editing codes).
      $description = strip_tags($description);
      // Max length is 35 characters.
      $description = substr($description, 0, 35);
      $transaction_request = $this->connector->createTransactionRequest($issuer, $return_url, $purchase_id, $amount, $description);
      $transaction_response = $transaction_request->send();
    }
    catch (IDealResponseException $e) {
      $this->logger->error("Ideal transaction request failed due to error @error. Details: @details", ['@error' => $e->getErrorMessage(), '@details' => $e->getErrorDetail()]);
      throw new InvalidRequestException($e->getConsumerMessage());
    }
    catch (\Exception $e) {
      $this->logger->error("Ideal transaction request failed due to error @error", ['@error' => $e->getMessage()]);
      throw new PaymentGatewayException(self::GENERAL_ERROR);
    }

    /** @var \DateTime $timeout */
    $timeout = $transaction_request->getTimeoutAt();

    return [
      'issuerAuthenticationURL' => $transaction_response->getAuthenticationUrl(),
      'transactionID' => $transaction_response->getTransactionId(),
      'transactionCreateDateTimestamp' => $transaction_response->getTransactionDateTime()->getTimestamp(),
      'expiredAt' => $timeout->getTimestamp(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function sendStatusRequest($transaction_id) {
    try {
      $status_request = $this->connector->createStatusRequest($transaction_id);
      $status_response = $status_request->send();
    }
    catch (IDealResponseException $e) {
      $this->logger->error("Ideal status request failed due to error: @error. Details: @details", ['@error' => $e->getErrorMessage(), '@details' => $e->getErrorDetail()]);
      throw new InvalidRequestException($e->getConsumerMessage());
    }
    catch (\Exception $e) {
      $this->logger->error("Ideal status request failed due to error @error", ['@error' => $e->getMessage()]);
      throw new PaymentGatewayException(self::GENERAL_ERROR);
    }

    return [
      'transactionID' => $status_response->getTransactionId(),
      'status' => $status_response->getStatus(),
      'statusDateTimestamp' => $status_response->getStatusDateTime(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getTransactionStatus($transaction_id) {
    $status_response = $this->sendStatusRequest($transaction_id);

    return $status_response['status'];
  }

}
