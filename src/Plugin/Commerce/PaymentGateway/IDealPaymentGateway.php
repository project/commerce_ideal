<?php

namespace Drupal\commerce_ideal\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_ideal\Entity\TransactionInterface;
use Drupal\commerce_ideal\IDealConnectorFactoryInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides the iDEAL off-site payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "ideal_payment_gateway",
 *   label = @Translation("iDEAL"),
 *   display_label = @Translation("iDEAL"),
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_ideal\PluginForm\RedirectCheckoutForm",
 *   },
 *   requires_billing_information = FALSE,
 * )
 */
class IDealPaymentGateway extends OffsitePaymentGatewayBase {

  use LoggerChannelTrait;
  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The ID of the parent config entity.
   *
   * Not available while the plugin is being configured.
   *
   * @var string
   */
  protected $entityId;

  /**
   * The payment type used by the gateway.
   *
   * @var \Drupal\commerce_payment\Plugin\Commerce\PaymentType\PaymentTypeInterface
   */
  protected $paymentType;

  /**
   * The payment method types handled by the gateway.
   *
   * @var \Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeInterface[]
   */
  protected $paymentMethodTypes;

  /**
   * The time.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The iDEAL connector factory.
   *
   * @var \Drupal\commerce_ideal\IDealConnectorFactoryInterface
   */
  protected $connectorFactory;

  /**
   * The iDEAL connector.
   *
   * @var null|\Drupal\commerce_ideal\IDealConnectorInterface
   */
  protected $ideal = NULL;

  /**
   * Constructs a new PaymentGatewayBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_payment\PaymentTypeManager $payment_type_manager
   *   The payment type manager.
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   *   The payment method type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \Drupal\commerce_ideal\IDealConnectorFactoryInterface $connector_factory
   *   The iDEAL connector factory.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, IDealConnectorFactoryInterface $connector_factory) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $entity_type_manager,
      $payment_type_manager,
      $payment_method_type_manager,
      $time
    );

    $this->connectorFactory = $connector_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('commerce_ideal.connector_factory')
    );
  }

  /**
   * Gets the iDeal connector object.
   *
   * @return \Drupal\commerce_ideal\IDealConnectorInterface
   *   The iDEAL connector
   */
  public function getIdealConnector() {
    if (!isset($this->ideal)) {
      $configuration = $this->getConfiguration();
      $this->ideal = $this->connectorFactory->create($configuration);
    }

    return $this->ideal;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'acquire_url' => '',
      'merchant_id' => '',
      'sub_id' => '0',
      'path_to_acquirer_certificate' => '',
      'path_to_merchant_certificate' => '',
      'path_to_merchant_private_key' => '',
      'merchant_private_key_password' => '',
      'order_id_prefix' => '',
      'order_id_suffix' => '',
      'get_issuers_period' => '30',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['acquire_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Acquirer url'),
      '#default_value' => $this->configuration['acquire_url'],
    ];
    $form['merchant_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('MerchantID'),
      '#description' => $this->t('MerchantID as supplied to the Merchant by the Acquirer. If the MerchantID has less than 9 digits, leading zeros must be used to fill out the field'),
      '#default_value' => $this->configuration['merchant_id'],
    ];
    $form['sub_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant subID'),
      '#description' => $this->t('Merchant subID, as supplied to the Merchant by the Acquirer, if the Merchant has requested to use this. A Merchant can state a request to the Acquirer to use one or more subIDs. In this way apart from the Legal Name the Trade name will also be shown on the bank statements for each subID used. Unless agreed otherwise with the Acquirer, the Merchant has to use 0 (zero) as subID by default (if no subIDs are used).'),
      '#default_value' => $this->configuration['sub_id'],
    ];
    $form['path_to_acquirer_certificate'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path to the acquirer certificate file'),
      '#description' => $this->t('Use absolute path. Important: This file should not be accessible directly by the web server!'),
      '#default_value' => $this->configuration['path_to_acquirer_certificate'],
    ];
    $form['path_to_merchant_certificate'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path to the merchant certificate file'),
      '#description' => $this->t('Use absolute path. Important: This file should not be accessible directly by the web server!'),
      '#default_value' => $this->configuration['path_to_merchant_certificate'],
    ];
    $form['path_to_merchant_private_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path to the merchant private key file'),
      '#description' => $this->t('Use absolute path. Important: This file should not be accessible directly by the web server!'),
      '#default_value' => $this->configuration['path_to_merchant_private_key'],
    ];
    $form['merchant_private_key_password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant private key password'),
      '#default_value' => $this->configuration['merchant_private_key_password'],
    ];
    $form['order_id_prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Order Prefix"),
      '#description' => $this->t("E.g. prefix 'ideal-2018' with order id 17 will be send as 'ideal-2018-17'."),
      '#default_value' => $this->configuration['order_id_prefix'],
    ];
    $form['order_id_suffix'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Order Suffix"),
      '#description' => $this->t("E.g. suffix '-ideal-2018' with order id 17 will be send as '17-ideal-2018'."),
      '#default_value' => $this->configuration['order_id_suffix'],
    ];
    $form['get_issuers_period'] = [
      '#type' => 'textfield',
      '#title' => $this->t('The Issuer list expiration period in days.'),
      '#description' => $this->t('The Issuer list should be updated wihtin one month (preferably earlier).'),
      '#default_value' => $this->configuration['get_issuers_period'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    if ($form_state->getErrors()) {
      return;
    }
    $values = $form_state->getValue($form['#parents']);
    $keys = [
      'path_to_acquirer_certificate',
      'path_to_merchant_certificate',
      'path_to_merchant_private_key',
    ];

    foreach ($keys as $key) {
      $file = $values[$key];
      if (!empty($file)) {
        if (!file_exists($file) || !is_readable($file)) {
          $message = $this->t('Cannot access file :filename.', [':filename' => $file]);
          $this->messenger()->addError($message);
          $form_state->setError($form[$key]);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['acquire_url'] = $values['acquire_url'];
      $this->configuration['merchant_id'] = $values['merchant_id'];
      $this->configuration['sub_id'] = $values['sub_id'];
      $this->configuration['path_to_acquirer_certificate'] = $values['path_to_acquirer_certificate'];
      $this->configuration['path_to_merchant_certificate'] = $values['path_to_merchant_certificate'];
      $this->configuration['path_to_merchant_private_key'] = $values['path_to_merchant_private_key'];
      $this->configuration['merchant_private_key_password'] = $values['merchant_private_key_password'];
      $this->configuration['get_issuers_period'] = $values['get_issuers_period'];
      $this->configuration['order_id_prefix'] = $values['order_id_prefix'];
      $this->configuration['order_id_suffix'] = $values['order_id_suffix'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    /** @var \Drupal\commerce_payment\PaymentStorageInterface $payment_storage */
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $transaction_id = $request->query->get('trxid');
    $payment = $payment_storage->loadByRemoteId($transaction_id);
    $ideal = $this->getIdealConnector();
    $status = $ideal->getTransactionStatus($transaction_id);
    $this->paymentProcessRemoteStatus($payment, $status);
    if ($status == $ideal::REMOTE_STATUS_OPEN) {
      $this->messenger()->addMessage($this->t("We didn't receive a confirmation from your bank. When you see that your payment has been completed, we will deliver your order after we have received the payment."), 'warning');
    }
    if ($status != $ideal::REMOTE_STATUS_SUCCESS && $status != $ideal::REMOTE_STATUS_OPEN) {
      $message = $this->t('Payment failed for the order @order_id because iDEAL responded with "@status" status for the transaction @trxid',
        [
          '@order_id' => $order->id(),
          '@status' => $status,
          '@trxid' => $transaction_id,
        ]
      );
      throw new PaymentGatewayException($message);
    }
  }

  /**
   * Apply the iDEAL status to the payment.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment entity.
   * @param string $status
   *   The iDEAL status.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function paymentProcessRemoteStatus(PaymentInterface $payment, $status) {
    $ideal = $this->getIdealConnector();
    $payment->set('remote_state', $status);

    switch ($status) {
      case $ideal::REMOTE_STATUS_SUCCESS:
        $payment->setState('completed');
        break;

      case $ideal::REMOTE_STATUS_OPEN:
        $payment->setState('authorization');
        break;

      case $ideal::REMOTE_STATUS_CANCELLED:
      case $ideal::REMOTE_STATUS_FAILURE:
        $payment->setState('authorization_voided');
        break;

      case $ideal::REMOTE_STATUS_EXPIRED:
        $payment->setState('authorization_expired');
        break;
    }

    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function onCancel(OrderInterface $order, Request $request) {
    parent::onCancel($order, $request);
    /** @var \Drupal\commerce_payment\PaymentStorageInterface $payment_storage */
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $transaction_id = $request->query->get('trxid');
    $payment = $payment_storage->loadByRemoteId($transaction_id);
    $payment->set('state', 'authorization_voided');
    $payment->save();
  }

  /**
   * Check the iDEAL "collection duty".
   *
   * The Merchant has to initiate a StatusRequest when the Consumer returns to
   * the page to which the Consumer was redirected by the Issuer
   * (the merchantReturnURL from the TransactionRequest).
   * However, it is possible for the Consumer to close browser before returning
   * to this merchantReturnURL (even when the Consumer has authenticated
   * the payment). It is mandatory for Merchants to perform a
   * StatusRequest for every transaction, also in case the Consumer does not
   * return to the Merchant's website. The iDEAL protocol prescribes a so called
   * "collection duty" for the result of every transaction.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function transactionsCollectionDuty() {
    $ideal = $this->getIdealConnector();
    /** @var \Drupal\commerce_ideal\TransactionStorageInterface $transaction_storage */
    $transaction_storage = $this->entityTypeManager->getStorage('commerce_ideal_transaction');
    $transactions = $transaction_storage->loadMultipleByStatus($ideal::REMOTE_STATUS_OPEN);

    foreach ($transactions as $transaction) {
      if ($this->checkCollectionDutyRules($transaction)) {
        $status = $ideal->getTransactionStatus($transaction->getTrxId());
        $payment = $transaction->getPayment();
        if ($status != $ideal::REMOTE_STATUS_OPEN && $payment instanceof PaymentInterface) {
          $this->paymentProcessRemoteStatus($payment, $status);
        }
        $transaction->setStatus($status);
        $transaction->save();
      }
    }
  }

  /**
   * The check for "collection duty" rules.
   *
   * If the retrieved status of a transaction is "Open", the StatusRequest for
   * this transaction has to be repeated after a short period of time. All other
   * statuses (Cancelled, Expired, Success and Failure) are final statuses.
   * Since these statuses cannot change anymore, it is not necessary
   * (and not allowed) to perform another StatusRequest. To avoid unnecessary
   * load on the iDEAL systems, Merchants shall not perform unnecessary
   * StatusRequests.
   *
   * @param \Drupal\commerce_ideal\Entity\TransactionInterface $transaction
   *   The transaction.
   *
   * @return bool
   *   TRUE if the check is passed, and FALSE if not.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function checkCollectionDutyRules(TransactionInterface $transaction) {
    $now = $this->time->getCurrentTime();
    $expired = ($now > $transaction->getExpiredTime()) ? TRUE : FALSE;

    if (!$this->checkCollectionDutyCommonRule($transaction, $now)) {
      return FALSE;
    }

    if ($expired && !$this->checkCollectionDutyExpiredRule($transaction, $now)) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * The check for common "collection duty" rules.
   *
   * @param \Drupal\commerce_ideal\Entity\TransactionInterface $transaction
   *   The transaction.
   * @param null|int $now
   *   (optional) The current timestamp.
   *
   * @return bool
   *   TRUE if the check is passed, and FALSE if not.
   */
  protected function checkCollectionDutyCommonRule(TransactionInterface $transaction, $now = NULL) {
    if (!isset($now)) {
      $now = $this->time->getCurrentTime();
    }
    // Do NOT perform repeated StatusRequests with
    // a time interval shorter than 60 second.
    if ($now - $transaction->getChangedTime() < 60) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * The check for expired "collection duty" rules.
   *
   * @param \Drupal\commerce_ideal\Entity\TransactionInterface $transaction
   *   The transaction.
   * @param null|int $now
   *   (optional) The current timestamp.
   *
   * @return bool
   *   TRUE if the check is passed, and FALSE if not.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function checkCollectionDutyExpiredRule(TransactionInterface $transaction, $now = NULL) {
    if (!isset($now)) {
      $now = $this->time->getCurrentTime();
    }

    // Do NOT perform repeated StatusRequests with
    // a time interval shorter than 60 minutes.
    if ($now - $transaction->getChangedTime() < 60 * 60) {
      return FALSE;
    }

    // If the "Open" status is still returned after the expiration period,
    // this can indicate a system failure. If this failure is not solved
    // within 24 hours, log warning message and stop sending StatusRequests.
    if ($now - $transaction->getExpiredTime() > 60 * 60 * 24) {
      $message = $this->t('Transaction @trxid has the "Open" status within 24 hours after the expiration period and this can indicate a system failure, please contact the Acquirer.', ['@trxid' => $transaction->getTrxId()]);
      $this->getLogger('commerce_ideal')->warning($message);
      return FALSE;
    }

    // Do NOT perform StatusRequests for transactions
    // with a timestamp older than 7 days.
    if ($now - $transaction->getCreatedTime() > 60 * 60 * 24 * 7) {
      $payment = $transaction->getPayment();
      if ($payment instanceof PaymentInterface) {
        $payment->set('state', 'authorization_voided');
        $payment->save();
      }
      $trxid = $transaction->getTrxId();
      $transaction->delete();
      $message = $this->t('Transaction @trxid has been deleted because a timestamp older than 7 days but the "Open" status returned.', ['@trxid' => $trxid]);
      $this->getLogger('commerce_ideal')->error($message);
      return FALSE;
    }

    return TRUE;
  }

}
