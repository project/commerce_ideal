<?php

namespace Drupal\commerce_ideal\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Transaction entity.
 *
 * @ingroup commerce_ideal
 *
 * @ContentEntityType(
 *   id = "commerce_ideal_transaction",
 *   label = @Translation("iDEAL Transaction"),
 *   base_table = "commerce_ideal_transaction",
 *   entity_keys = {
 *     "id" = "id",
 *     "trxid" = "trxid",
 *   },
 *   handlers = {
 *     "storage" = "Drupal\commerce_ideal\TransactionStorage",
 *   },
 * )
 */
class Transaction extends ContentEntityBase implements TransactionInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function getTrxId() {
    return $this->get('trxid')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTrxId($id) {
    $this->set('trxid', $id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus() {
    return $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($id) {
    $this->set('status', $id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPayment() {
    return $this->get('payment_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentId() {
    return $this->get('payment_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getExpiredTime() {
    return $this->get('expired')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setExpiredTime($timestamp) {
    $this->set('expired', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['trxid'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Transaction ID'))
      ->setDescription(t('Unique 16-digit number within iDEAL.'))
      ->setSetting('max_length', 16)
      ->setRequired(TRUE);

    $fields['status'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Status'))
      ->setDescription(t('Indicates one of the iDEAL statuses of the transaction'))
      ->setSetting('max_length', 9)
      ->setRequired(TRUE);

    $fields['payment_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Payment'))
      ->setDescription(t('The parent commerce payment.'))
      ->setSetting('target_type', 'commerce_payment')
      ->setReadOnly(TRUE);

    $fields['expired'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Expired'))
      ->setDescription(t('The period of validity of the payment request.'));

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the transaction was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the transaction was last edited.'));

    return $fields;
  }

}
