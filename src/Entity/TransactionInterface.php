<?php

namespace Drupal\commerce_ideal\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface for defining Transaction entities.
 *
 * @ingroup commerce_ideal
 */
interface TransactionInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   * Gets the Transaction ID.
   *
   * @return string
   *   Id of the Transaction.
   */
  public function getTrxId();

  /**
   * Sets the Transaction ID.
   *
   * @param string $id
   *   The Transaction name.
   *
   * @return \Drupal\commerce_ideal\Entity\TransactionInterface
   *   The called Transaction entity.
   */
  public function setTrxId($id);

  /**
   * Gets the Transaction status.
   *
   * @return string
   *   Status of the Transaction.
   */
  public function getStatus();

  /**
   * Sets the Transaction status.
   *
   * @param string $status
   *   The Transaction status.
   *
   * @return \Drupal\commerce_ideal\Entity\TransactionInterface
   *   The called Transaction entity.
   */
  public function setStatus($status);

  /**
   * Gets the parent payment.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentInterface|null
   *   The payment entity, or null.
   */
  public function getPayment();

  /**
   * Gets the parent payment ID.
   *
   * @return int|null
   *   The payment ID, or null.
   */
  public function getPaymentId();

  /**
   * Gets the Transaction creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Transaction.
   */
  public function getCreatedTime();

  /**
   * Sets the Transaction creation timestamp.
   *
   * @param int $timestamp
   *   The Transaction creation timestamp.
   *
   * @return \Drupal\commerce_ideal\Entity\TransactionInterface
   *   The called Transaction entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Transaction expiration timestamp.
   *
   * @return int
   *   Creation timestamp of the Transaction.
   */
  public function getExpiredTime();

  /**
   * Sets the Transaction expiration timestamp.
   *
   * @param int $timestamp
   *   The Transaction creation timestamp.
   *
   * @return \Drupal\commerce_ideal\Entity\TransactionInterface
   *   The called Transaction entity.
   */
  public function setExpiredTime($timestamp);

}
