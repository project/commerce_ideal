<?php

namespace Drupal\commerce_ideal\PluginForm;

use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\commerce_ideal\Entity\Transaction;
use Drupal\commerce_ideal\IDealConnectorFactoryInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the Off-site form for iDeal Checkout.
 */
class RedirectCheckoutForm extends PaymentOffsiteForm implements ContainerInjectionInterface {

  /**
   * The state storage.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $stateStorage;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The iDEAL connector service.
   *
   * @var \Drupal\commerce_ideal\IDealConnectorFactoryInterface
   */
  protected $connectorFactory;

  /**
   * Constructs a new RedirectCheckoutForm object.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state storage.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger service.
   * @param \Drupal\commerce_ideal\IDealConnectorFactoryInterface $connector_factory
   *   The iDEAL connector factory service.
   */
  public function __construct(StateInterface $state, LoggerChannelFactoryInterface $logger_factory, IDealConnectorFactoryInterface $connector_factory) {
    $this->stateStorage = $state;
    $this->logger = $logger_factory->get('commerce_ideal');
    $this->connectorFactory = $connector_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('state'),
      $container->get('logger.factory'),
      $container->get('commerce_ideal.connector_factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->getEntity();
    $config = $this->getPluginConfiguration($payment);
    $ideal = $this->connectorFactory->create($config);
    $issuers = $ideal->getIssuers();
    $form['issuers'] = [
      '#type' => 'select',
      '#title' => $this
        ->t('Choose a bank'),
      '#options' => $issuers,
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Complete payment'),
    ];
    $form['actions']['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#url' => Url::fromUri($form['#cancel_url']),
    ];

    return $form;

  }

  /**
   * Gets the iDEAL payment plugin configuration.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment entity.
   *
   * @return array
   *   An array of this plugin's configuration.
   */
  private function getPluginConfiguration(PaymentInterface $payment) {
    $plugin = $payment->getPaymentGateway()->getPlugin();

    return $plugin->getConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue($form['#parents']);
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->getEntity();
    $config = $this->getPluginConfiguration($payment);
    $amount = $payment->getAmount()->getNumber();
    // Convert amount to cents.
    $amount *= 100;
    $purchase_id = $config['order_id_prefix'] . $payment->getOrderId() . $config['order_id_suffix'];
    $return_url = $form['#return_url'];
    $issuer = $values['issuers'];
    $order = $payment->getOrder();
    $store = $order->getStore()->getName();
    $description = substr($store, 0, 35);

    $ideal = $this->connectorFactory->create($config);
    $response_data = $ideal->sendTransactionRequest($issuer, $return_url, $purchase_id, $amount, $description);

    $payment->setRemoteId($response_data['transactionID']);
    $payment->setState('authorization');
    $payment->setExpiresTime($response_data['expiredAt']);
    $payment->save();

    $transaction = Transaction::create([
      'trxid' => $response_data['transactionID'],
      'status' => $ideal::REMOTE_STATUS_OPEN,
      'payment_id' => $payment->id(),
      'expired' => $response_data['expiredAt'],
      'created' => $response_data['transactionCreateDateTimestamp'],
      'changed' => $response_data['transactionCreateDateTimestamp'],
    ]);
    $transaction->save();

    $issuer_url = $response_data['issuerAuthenticationURL'];
    throw new NeedsRedirectException($issuer_url);
  }

}
