<?php

namespace Drupal\commerce_ideal;

/**
 * Interface IdealInterface.
 */
interface IDealConnectorInterface {

  /**
   * The success iDEAL payment status.
   *
   * Positive result; the payment is guaranteed.
   *
   * @string
   */
  const REMOTE_STATUS_SUCCESS = 'Success';

  /**
   * The cancelled iDEAL payment status.
   *
   * Negative result due to cancellation by Consumer;
   * no payment has been made.
   *
   * @string
   */
  const REMOTE_STATUS_CANCELLED = 'Cancelled';

  /**
   * The expired iDEAL payment status.
   *
   * Negative result due to expiration of the transaction;
   * no payment has been made.
   *
   * @string
   */
  const REMOTE_STATUS_EXPIRED = 'Expired';

  /**
   * The failure iDEAL payment status.
   *
   * Negative result due to other reasons;
   * no payment has been made.
   *
   * @string
   */
  const REMOTE_STATUS_FAILURE = 'Failure';

  /**
   * The open iDEAL payment status.
   *
   * Final result not yet known.
   * A new status request is necessary to obtain the status.
   *
   * @string
   */
  const REMOTE_STATUS_OPEN = 'Open';

  /**
   * Returns a list of issuing banks.
   *
   * @return array
   *   An array of all issuers.
   */
  public function getIssuers();

  /**
   * The XML message to initiate the payment.
   *
   * @param string $issuer
   *   The ID (BIC) of the Issuer selected by the Consumer.
   * @param string $return_url
   *   URL to redirect the after authorisation of the transaction.
   * @param string $purchase_id
   *   Unique identification of the order within the Merchant's system.
   * @param int $amount
   *   The amount payable in euro (cents).
   * @param string $description
   *   Description of the product(s) or services being paid for.
   *
   * @return array
   *   An array of fields of the TransactionResponse message.
   */
  public function sendTransactionRequest($issuer, $return_url, $purchase_id, $amount, $description);

  /**
   * The XML message to fetch the status of the transaction.
   *
   * @param string $transaction_id
   *   Unique 16-digit number code of the transaction.
   *
   * @return array
   *   An array fields of the StatusResponse message.
   */
  public function sendStatusRequest($transaction_id);

  /**
   * The iDEAL transaction status string.
   *
   * @param string $transaction_id
   *   Unique 16-digit number code of the transaction.
   *
   * @return string
   *   Status of the transaction.
   */
  public function getTransactionStatus($transaction_id);

}
