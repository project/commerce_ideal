<?php

namespace Drupal\commerce_ideal;

use Drupal\Core\State\StateInterface;

/**
 * Class ConnectorFactory.
 */
class IDealConnectorFactory implements IDealConnectorFactoryInterface {

  /**
   * The state storage.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  private $state;

  /**
   * Constructs a new ConnectorFactory object.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   Drupal state storage.
   */
  public function __construct(StateInterface $state) {
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public function create(array $config) {
    return new IDealConnector($config, $this->state);
  }

}
