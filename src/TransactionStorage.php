<?php

namespace Drupal\commerce_ideal;

use Drupal\commerce\CommerceContentEntityStorage;

/**
 * Defines the transaction storage.
 */
class TransactionStorage extends CommerceContentEntityStorage implements TransactionStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function loadByTrxId($trxid) {
    $transactions = $this->loadByProperties(['trxid' => $trxid]);
    $transaction = reset($transactions);

    return $transaction ?: NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function loadMultipleByStatus($status) {
    $query = $this->getQuery()
      ->condition('status', $status)
      ->sort('changed');
    $result = $query->execute();

    return $result ? $this->loadMultiple($result) : [];
  }

}
