<?php

namespace Drupal\commerce_ideal\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Drupal\Console\Core\Command\Command;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Class TransactionsCheckCommand.
 *
 * Drupal\Console\Annotations\DrupalCommand (
 *     extension="commerce_ideal",
 *     extensionType="module"
 * )
 */
class TransactionsCheckCommand extends Command {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new TransactionsCheckCommand object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
    parent::__construct();
  }

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('commerce_ideal:transactions:check')
      ->setDescription($this->trans('commands.commerce_ideal.transactions.check.description'));
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    $this->getIo()->info('execute');
    $gateway_storage = $this->entityTypeManager->getStorage('commerce_payment_gateway');
    $gateways = $gateway_storage->loadByProperties([
      'plugin' => 'ideal_payment_gateway',
    ]);

    $gateway = reset($gateways);
    /** @var \Drupal\commerce_ideal\Plugin\Commerce\PaymentGateway\IDealPaymentGateway $plugin */
    $plugin = $gateway->getPlugin();
    $plugin->transactionsCollectionDuty();
    $this->getIo()->info($this->trans('commands.commerce_ideal.transactions.check.messages.success'));
  }

}
