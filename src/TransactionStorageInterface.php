<?php

namespace Drupal\commerce_ideal;

use Drupal\Core\Entity\ContentEntityStorageInterface;

/**
 * Defines the interface for transaction storage.
 */
interface TransactionStorageInterface extends ContentEntityStorageInterface {

  /**
   * Loads the transaction for the given remote trx id.
   *
   * @param string $trxid
   *   The iDEAL transaction ID.
   *
   * @return \Drupal\commerce_ideal\Entity\TransactionInterface|null
   *   The transaction, or NULL if none found.
   */
  public function loadByTrxId($trxid);

  /**
   * Loads all transaction for the given status.
   *
   * @param string $status
   *   The iDEAL transaction status.
   *
   * @return \Drupal\commerce_ideal\Entity\TransactionInterface[]
   *   The transactions.
   */
  public function loadMultipleByStatus($status);

}
