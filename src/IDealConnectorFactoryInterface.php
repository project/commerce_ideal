<?php

namespace Drupal\commerce_ideal;

/**
 * An iDEAL connector factory interface.
 */
interface IDealConnectorFactoryInterface {

  /**
   * Creates the connector for the iDEAL payment gateway.
   *
   * @param array $config
   *   The payment gateway plugin configuration.
   *
   * @return \Drupal\commerce_ideal\IDealConnectorInterface
   *   The iDEAL connector.
   */
  public function create(array $config);

}
