CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers


INTRODUCTION
------------

This module integrates iDEAL payments into Drupal Commerce.

iDEAL is an online payment method that enables consumers to pay online through
their own bank. In addition to webshops, other organisations that are not part
of the e-commerce market also offer iDEAL. iDEAL is increasingly used to pay
energy bills, make donations to charities, buy mobile credits, pay local taxes,
traffic fines, etc.

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/commerce_ideal

* To submit bug reports and feature suggestions, or to track changes:
  https://www.drupal.org/project/issues/commerce_ideal


REQUIREMENTS
------------

This module requires following modules outside of Drupal core:

* Drupal Commerce - https://www.drupal.org/project/commerce


INSTALLATION
------------

* Install the Commerce iDEAL Payment Gateway module with composer
  composer require drupal/commerce_ideal.
  Visit https://www.drupal.org/docs/develop/using-composer/using-composer-to-install-drupal-and-manage-dependencies#managing-contributed
  for further information.


CONFIGURATION
-------------

Visit https://www.drupal.org/docs/8/modules/commerce-ideal-payment-gateway
  for further information.
1. Navigate to Administration > Extend and enable the module.
2. Navigate to Administration > Commerce > Configuration > Payment gateways.
3. Add payment gateway. Select iDEAL Plugin and fill in the form. Save.
4. Add new cron task to execute "commerce_ideal:transactions:check" Drupal console
 command at every 3rd minute, e.g.: */3 * * * * drupal commerce_ideal:transactions:check 
     

MAINTAINERS
-----------

* Alexey Sivets (w.drupal) - https://www.drupal.org/u/wdrupal

Supporting organizations:

* Engine Eight - https://www.drupal.org/engine-eight
